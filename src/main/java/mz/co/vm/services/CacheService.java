package mz.co.vm.services;

import javax.enterprise.context.ApplicationScoped;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

import mz.co.vm.models.RandomNumber;

@ApplicationScoped
public class CacheService {
	 
    private CacheManager cacheManager;
    private Cache<String, RandomNumber> cacheRandomNumbers;
    private Cache<String, Double> cacheTimeRequest;
 
    public CacheService() {
        cacheManager = CacheManagerBuilder
          .newCacheManagerBuilder().build();
        cacheManager.init();
 
        cacheRandomNumbers = cacheManager
          .createCache("cache_numbers", CacheConfigurationBuilder
            .newCacheConfigurationBuilder(
              String.class, RandomNumber.class,
              ResourcePoolsBuilder.heap(10)));
        
        cacheTimeRequest = cacheManager
                .createCache("cache_time", CacheConfigurationBuilder
                  .newCacheConfigurationBuilder(
                    String.class, Double.class,
                    ResourcePoolsBuilder.heap(10)));
    }
 
    public Cache<String, RandomNumber> getRandomNumberCacheFromCacheManager() {
        return cacheManager.getCache("cache_numbers", String.class, RandomNumber.class);
    }
    
    public Cache<String, Double> getTimeRequestCacheFromCacheManager() {
        return cacheManager.getCache("cache_time", String.class, Double.class);
    }
}