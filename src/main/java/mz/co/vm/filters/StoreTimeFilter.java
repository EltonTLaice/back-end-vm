package mz.co.vm.filters;

import java.io.IOException;
import java.time.Instant;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.ext.Provider;

import org.ehcache.Cache;

import mz.co.vm.interfaces.Duration;
import mz.co.vm.interfaces.StoreTime;
import mz.co.vm.models.RandomNumber;
import mz.co.vm.services.CacheService;
import mz.co.vm.services.PrngService;

/**
 * The DurationFilter Class represents the filter that treats requests 
 * and replenishes every application, allowing to calculate the duration 
 * of the requests
 * 
 * 
 * @author Elton Tomas Laice
 * @see mz.co.vm.interfaces.Duration
 *
 */

@Provider
@StoreTime
@ApplicationScoped
public class StoreTimeFilter implements ContainerRequestFilter, ContainerResponseFilter{
	@Inject
	private CacheService cacheService;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {		
		/**
		 * Store Total Time of Request
		 */
		String requestId = responseContext.getHeaderString("Request-Id");
		String requestDuration = responseContext.getHeaderString("X-Request-Duration");
		Cache<String, RandomNumber> cache = cacheService.getRandomNumberCacheFromCacheManager();
		cacheService.getTimeRequestCacheFromCacheManager().put(requestId, Double.parseDouble(requestDuration));
	}
}
