package mz.co.vm.boot;


import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import mz.co.vm.services.CacheService;

/**
 * 
 * @author Eton Tomas Laice
 *
 */
@ApplicationPath("/")
public class JaxrsActivator extends Application {
	@Inject
	private CacheService cacheService;
}
