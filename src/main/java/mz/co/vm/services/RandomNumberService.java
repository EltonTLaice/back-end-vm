package mz.co.vm.services;

import java.util.Random;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonObject;

import mz.co.vm.models.RandomNumber;

@ApplicationScoped
public class RandomNumberService {
	public JsonObject getJsonRandomNumber(RandomNumber randomNumber) {
    	JsonObject model = Json.createObjectBuilder()
 			   .add("requestID", randomNumber.getRequestID())
 			   .add("number", randomNumber.getNumber())
 			   .add("process_time", randomNumber.getProcess_time())
 			   .build();
    	return model;
	}
}
