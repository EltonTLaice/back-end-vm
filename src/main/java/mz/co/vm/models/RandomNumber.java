package mz.co.vm.models;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


public class RandomNumber implements Serializable{

	private String requestID;
	
	private long number;
	
	private double process_time;
	
	public RandomNumber() {}
	public RandomNumber(String requestID) {
		this.requestID = requestID;
		gen_numeber();
	}
	public String getRequestID() {
		return requestID;
	}

	public long getNumber() {
		return number;
	}

	public double getProcess_time() {
		return process_time;
	}
	public void setProcess_time(double process_time) {
		this.process_time = process_time;
	}
	public void setProcess_time(int process_time) {
		this.process_time = process_time;
	}
	
	private void gen_numeber() {
		long number;
		
		do {
			Random rn = new Random();
			number = rn.nextLong();
		} while (number < 3);
		
		this.number = number;
	}
}
