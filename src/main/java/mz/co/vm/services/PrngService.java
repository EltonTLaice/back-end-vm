package mz.co.vm.services;

import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.ehcache.Cache;

import mz.co.vm.models.RandomNumber;

/**
 * Services for PRNG resources
 * 
 * @author Elton Tomas Laice
 * @see mz.co.vm.controllers.PrngController
 */
@ApplicationScoped
public class PrngService {
	
	@Inject
	private CacheService cacheService;
	/**
	 * Success information if random number was created
	 * @return json
	 */
	public JsonObject success_JsonRandomNumber(String requestId) {
    	JsonObject json = Json.createObjectBuilder()
 			   .add("status", "success")
 			   .add("requestId", requestId)
 			   .add("message", "Request created.")
 			   .build();
    	return json;
	}
	
	/**
	 * The IDs generated must be 128-bit GUIDs
	 * @return requestID
	 */
	public String create_id_request() {
		UUID uuid = UUID.randomUUID();
		String requestID = uuid.toString();
		return requestID;
	}
	
	/**
	 * Store Number class in H2 database memory
	 * @param RequestId
	 */
	public void createNumber(String requestId){
		RandomNumber randomNumber = new RandomNumber(requestId);
		if(cacheService.getTimeRequestCacheFromCacheManager().get(requestId) != null) 
			randomNumber.setProcess_time(cacheService.getTimeRequestCacheFromCacheManager().get(requestId));
	    cacheService.getRandomNumberCacheFromCacheManager().put(requestId, randomNumber);
	}
	
	/**
	 * Returns a List of Generated Random Numbers.
	 * This includes the time that was taken to process
	 * each random number.
	 * 
	 * @return JsonObject
	 */
	public JsonObject getHistory() {
		Iterator<Cache.Entry<String, RandomNumber>> iterator = cacheService.getRandomNumberCacheFromCacheManager().iterator();
		
		JsonArrayBuilder json_numbers = Json.createArrayBuilder();
		while (iterator.hasNext()) {
	        RandomNumber randomNumber = (RandomNumber) iterator.next().getValue();
	    	JsonObject json_object = Json.createObjectBuilder()
	  			   .add("requestID", randomNumber.getRequestID())
	  			   .add("number", randomNumber.getNumber())
	  			   .add("process_time", randomNumber.getProcess_time()).build();
	    	json_numbers.add(json_object);
	    }
		
		JsonObjectBuilder json_main = Json.createObjectBuilder()
				.add("status", "success")
				.add("random_numbers", json_numbers);
     	return json_main.build();
	}
}
